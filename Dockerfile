FROM java:8-jdk-alpine
#ENV dbprofile  -Dspring.profiles.active=h2
COPY ./devops_sample/target/*.jar /usr/app/
WORKDIR /usr/app/
EXPOSE 8090
ENTRYPOINT java -jar $dbprofile *.jar
