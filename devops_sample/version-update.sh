tag=$(git log -n 1 --format='%cd-%h' --abbrev=8 --date=short)
tag=$(echo $tag|awk '{ print substr($0, 3, 17)}')
tag=$(echo $tag|sed -e "s/-//;s/-//")
echo "$tag"

